-- Створити БД
CREATE DATABASE php_fundamentals;

-- Видалити БД
DROP DATABASE php_fundamentals;

-- Вивід всіх існуючих баз даних
SHOW DATABASES;

-- Вибір БД
USE php_fundamentals;

-- ##########################

-- Створення таблиці
CREATE TABLE users (
  id INT AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(20),
  created_at DATE
);

-- SHOW TABLES. Вивід всіх таблиць
SHOW TABLES;

-- EXPLAIN. Вивід структури таблиць
EXPLAIN users;

-- DROP TABLE. Виділення таблиці
DROP TABLE users;

-- ALTER TABLE. Зміна таблиці (додовання, зміна, видалення полів і їх типів)
ALTER TABLE users
  ADD email VARCHAR(100)
AFTER username;

-- ##########################

-- INSERT. Додавання інформації в таблицю
-- Є кілька варіантів додавання даних в таблицю.

INSERT INTO users
VALUES (NULL, 'User Name', '2017-04-11');

INSERT INTO users SET
username = 'User Name',
email = 'username@gmail.com',
created_at = '2017-04-11';

INSERT INTO users (`id`, `created_at`, `username`)
VALUES (NULL, '2017-04-11', 'User Name');

-- ##########################

SELECT LAST_INSERT_ID();

-- ##########################

-- SELECT. Читання даних з таблиці
SELECT * FROM users;
SELECT username, email FROM users;

-- Умова WHERE
SELECT email FROM users WHERE username = “User Name”;
SELECT * FROM users WHERE id <= 2;
SELECT * FROM users WHERE id != 2;
-- AND або OR можуть бути використані для об'єднання умов:
SELECT * FROM users WHERE id = 1 OR username = ‘User Name’;


-- ##########################

-- IN (), NOT IN ()
SELECT * FROM users WHERE date_created IN(‘2017-04-11’, ‘2017-04-12’);

-- LIKE
SELECT * FROM users WHERE email LIKE(‘%gmail%’);

-- ##########################

-- Умова ORDER BY
SELECT * FROM users ORDER BY created_at;
SELECT * FROM users ORDER BY created_at DESC;

-- ##########################

-- UPDATE. Внесення змін в інформацію в таблиці
UPDATE users SET
  username = 'User New Name',
  email = 'username_new @ gmail.com',
  created_at = '2017-04-12'
WHERE id = 1;

-- ##########################

-- DELETE. Видалення інформації з таблиці
DELETE FROM users WHERE id = 1;
DELETE FROM users;

-- ##########################

-- PRIMARY KEY
CREATE TABLE Persons (
  ID int NOT NULL,
  LastName varchar(255) NOT NULL,
  FirstName varchar(255),
  Age int,
  PRIMARY KEY (ID)
);

CREATE TABLE Persons (
  ID int NOT NULL,
  LastName varchar(255) NOT NULL,
  FirstName varchar(255),
  Age int,
  CONSTRAINT PK_Person PRIMARY KEY (ID,LastName)
);

-- ##########################

-- FOREIGN KEY
CREATE TABLE orders (
  id int NOT NULL AUTO_INCREMENT,
  number int NOT NULL,
  userId int,
  PRIMARY KEY (id),
  FOREIGN KEY (userId) REFERENCES users(id)
);

CREATE TABLE orders (
  id int NOT NULL AUTO_INCREMENT,
  number int NOT NULL,
  userId int,
  PRIMARY KEY (id),
  FOREIGN KEY (userId) REFERENCES users(id)
    ON UPDATE CASCADE
  ON DELETE RESTRICT
);




