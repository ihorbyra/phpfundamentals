<?php

$a = true;
$b = 1 === $a;

var_dump(test($a));
echo '<br>';
var_dump(test_short($a));
echo '<br>';
var_dump(test_shortest($b));

// не оптимізований запис
function test($a)
{
    if ($a) {
        return true;
    } else {
        return false;
    }
}

// кращий оптимізований запис
function test_short($a)
{
    if ($a) {
        return true;
    }
    return false;    // else не є обов'язковим
}

// або ще коротше
function test_shortest($a)
{
    return (bool) $a;
}
