<?php
echo test(2);    // Цей код виведе 'case 2' і 'case 3'.

function test($a)
{
    $answer = 'Вивід: ';
    switch ($a) {
        case 1:
            $answer .= 1;
            break;             // Припинить виконання switch тут якщо $a == 1, так як використовується 'break'
        case 2:
            // Код без 'break', тому буде виконано порівняння з 'case 3'
            $answer .= 2;
        case 3:
            $answer .= 3;
            return $answer;    // В середині функції 'return' завершить її роботу
        default:
            $answer .= 'default';
            return $answer;
    }

    return $answer;
}