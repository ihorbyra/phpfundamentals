<?php
$array1 = ["a" => "green", "red", "blue", "red"];
$array2 = ["b" => "green", "yellow", "red"];
$result = array_diff($array1, $array2);

echo 'Результат array_diff: <br>';
echo '<pre>';
var_dump($result);
echo '</pre>';

//------------------------------------------------------------------------------------------------------------------

$array1 = ["a" => "green", "b" => "brown", "c" => "blue", "red"];
$array2 = ["a" => "green", "yellow", "red"];
$result = array_diff_assoc($array1, $array2);

echo 'Результат array_diff_assoc: <br>';
echo '<pre>';
var_dump($result);
echo '</pre>';

//------------------------------------------------------------------------------------------------------------------

$array1 = ["a" => "green", "red", "blue"];
$array2 = ["b" => "green", "yellow", "red"];
$result = array_intersect($array1, $array2);

echo 'Результат array_intersect: <br>';
echo '<pre>';
var_dump($result);
echo '</pre>';

//------------------------------------------------------------------------------------------------------------------

$array1 = ["a" => "green", "b" => "brown", "c" => "blue", "red"];
$array2 = ["a" => "green", "yellow", "red"];
$result = array_intersect_assoc($array1, $array2);

echo 'Результат array_intersect_assoc: <br>';
echo '<pre>';
var_dump($result);
echo '</pre>';

