<?php

$a = 5;   // 5 є цілим числом, integer

var_dump($a == 5);       // Порівнюються значення; Поверне true
echo '<br>';
var_dump($a == '5');     // Порівнюються значення (ігнорує типи); Поверне true
echo '<br>';
var_dump($a === 5);      // Порівнюються значення і типи (integer vs. integer); Поверне true
echo '<br>';
var_dump($a === '5');    // Порівнюються значення і типи (integer vs. string); Поверне false
echo '<br>';

/**
 * Строге порівняння
 */

$myString = 'abcdef';
$findMe   = 'ab';
$pos = strpos($myString, $findMe); // http://php.net/manual/ru/function.strpos.php

if ($pos) { // 'a' знаходиться в 0 позиції, тому результат буде 'false'
    echo "Пошукова фраза '{$findMe}' знайдена в стрічці '{$myString}' на позиції {$pos}<br>";
} else {
    echo "Пошукова фраза '{$findMe}' не знайдена в стрічці '{$findMe}'<br>";
}

if ($pos !== false) { // Результат буде 'true', так як тут строге порівняння (0 !== false)
    echo "Пошукова фраза '{$findMe}' знайдена в стрічці '{$myString}' на позиції {$pos}<br>";
} else {
    echo "Пошукова фраза '{$findMe}' не знайдена в стрічці '{$findMe}'<br>";
}