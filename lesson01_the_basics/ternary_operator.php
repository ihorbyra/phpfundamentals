<?php
$value = true;

//Починаючи з версії PHP 5.3 можна використовувати тернарний оператор в такому варіанті:
$var = $value ?: "Варіант використання з PHP 5.3";
// еквівалентно
//$var = $value ? $value : "Варіант використання з PHP 5.3";
var_dump($var);
echo '<br>';

//------------------------------------------------------------------------------------------------------------------

//$var = $value ?? "Варіант використання з PHP 7";
// еквівалентно
//$var = isset($value) ? $value : "Варіант використання з PHP 7";

//------------------------------------------------------------------------------------------------------------------

$a = 5;
var_dump(ternaryWithReturn($a));
echo '<br>';

/*function ternaryWithError($a)
{
    echo ($a == 5) ? return true : return false;    // цей приклад буде видавати помідомлення про помилку
}*/

function ternaryWithReturn($a)
{
    return ($a == 5) ? true : false;    // цей приклад поверне true або false
}

//------------------------------------------------------------------------------------------------------------------

$b = 3;
echo 'Return ternary: ';
var_dump(returnTernary($b));
echo '<br>';
echo 'Return without ternary: ';
var_dump(returnWithoutTernary($b));
echo '<br>';

function returnWithoutTernary($b)
{
    return $b == 3;    // цей приклад поверне true або false
}

function returnTernary($b)
{
    return ($b == 3) ? true : false;    // цей приклад поверне true або false
}