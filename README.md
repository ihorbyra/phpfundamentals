#День 11. Об'єктно-орієнтоване програмування. Магічні методи. Наслідування
Приклади: **lesson11_OOP**

Матеріали:
будуть доступні 14 липня


#День 10. Об'єктно-орієнтоване програмування. Основні поняття: класи, об"єкти, робота з методами та властивостями. Конструктор, деструктор. Public, protected і private: управління доступом до класів
Приклади: **lesson10_OOP**

Матеріали:

- resources \ **День 10. ООП.pdf**


#День 9. Сесії
Приклади: **lesson09_sessions**

Матеріали:

- resources \ **День 9. PHP and Sessions.pdf**


#День 8. SQL. Робота з БД. PDO
Приклади: **lesson08_php_pdo**

Матеріали:

- resources \ **День 8. Робота з БД. PDO..pdf**


#День 7. SQL. Робота з БД. MySQLI
Приклади: **lesson07_php_mysqli**

Матеріали:

- resources \ **День 7. Робота з БД. MySQLI.pdf**

# Практичне завдання 
[Перейти до опису](https://bitbucket.org/ihorbyra/phpfundamentals/src/5c8407958034a732d38dac7b80674419a1d08228/project/?at=master)

#День 6. SQL. Об'єднання таблиць (SQL Joins)
Приклади: **lesson06_sql_joins**

Матеріали:

- resources \ **День 6. SQL. Об'єднання таблиць (SQL Joins).pdf**

#День 5. SQL. Вибір типів даних в Mysql.
Матеріали:

- resources \ **День 5. SQL. Вибір типів даних в Mysql.pdf**

#День 4. SQL. CRUD. PRIMARY KEY, FOREIGN KEY.
Приклади: **lesson04_sql_crud_keys**

Матеріали:
 
- resources \ **День 4. SQL.pdf**
- книга по MySQL resources \ **Learning MySQL.pdf**

#День 3. Підключення файлів. Автозавантаження.
Приклади: **lesson03_include_files**

Матеріали: resources \ **День 3. Підключення файлів.pdf**


- [require](http://php.net/manual/ru/function.require.php)
- [include](http://php.net/manual/ru/function.include.php)
- [require_​once](http://php.net/manual/ru/function.require-once.php)
- [include_​once](http://php.net/manual/ru/function.include-once.php)
- [Магічні константи](http://php.net/language.constants.predefined) 

####PSR-4: Autoloader 
- Опис http://www.php-fig.org/psr/psr-4/
- Приклад Реалізації PSR-4 https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader-examples.md

####PSR-0: Autoloading Standard
Для загального розуміння http://www.php-fig.org/psr/psr-0/ Цей стандарт позначено як Deprecated, PSR-4 - ренкомендована альтернатива

#День 1-2. Основи.

Приклади: **lesson01_the_basics**

Матеріали:

- resources \ **День 1. Основи.pdf**
- resources \ **День 1. Стандарти написання коду. PHP Standard Recommendation.pdf**
- resources \ **День 2. Основи.pdf**


####[Стандарти написання коду. PHP Standard Recommendation ](http://www.php-fig.org/)
- Конструкція IF/ELSE (оптимізація записів): **lesson01_the_basics/if_statements.php**
- switch: **lesson01_the_basics/switch.php**
- Вирішення проблем із операторарами порівння: **lesson01_the_basics/strict_comparisons.php**
- Тернарний оператор, оптимізація: **lesson01_the_basics/ternary_operator.php**
- Масиви
- Цикли
- Функції

#Курс розрахований на програмістів, які вже почали працювати з PHP.

Ви повинні знати основи:

- що таке змінні http://php.net/manual/ru/language.variables.php  і константи http://php.net/manual/ru/language.constants.php 
- область видимості змінних http://php.net/manual/ru/language.variables.scope.php 
- типи даних http://php.net/manual/ru/language.types.php і маніпуляція з ними http://php.net/manual/ru/language.types.type-juggling.php 
- оператори порівняння http://php.net/manual/ru/language.operators.comparison.php 
і їх пріоритети http://php.net/manual/ru/language.operators.precedence.php 
- побітові оператори http://php.net/manual/ru/language.operators.bitwise.php 
- управляючі конструкції http://php.net/manual/ru/language.control-structures.php , з них більш детально ми розглянемо require, include, require_once, include_once
- строкові функції http://php.net/manual/ru/ref.strings.php , хоча б такі як isset(), empty(), explode(), str_replace(), trim(), count() та ін.
- робота з файлами та файловою системою http://php.net/manual/ru/ref.filesystem.php
- опрацювання вхідних даних з форм http://php.net/manual/ru/language.variables.external.php 
- та ін.



# Консоль:
- [Основы линукс: Введение в bash](http://linuxgeeks.ru/bash-intro.htm)
- [The Beginner’s Guide to Nano, the Linux Command-Line Text Editor](https://www.howtogeek.com/howto/42980/the-beginners-guide-to-nano-the-linux-command-line-text-editor/)

