<?php
require_once "autoload.php";
spl_autoload_register(['Autoloader', 'loadPackages']);

use \app\ShoppingCart\ShoppingCart as ShoppingCart;

$cart = new ShoppingCart();
$catalog = new Catalog();
//Файл з функціоналом новин не підключено, хоча він існує)
// Для підключення розкоментуйте
//$news = new News();