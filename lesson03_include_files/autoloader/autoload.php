<?php
class Autoloader {
    private static $lastLoadedFilename;

    public static function loadPackages($className) {
        $namespase = false;

        if (strpos($className, "\\") !== false &&
            file_exists(__DIR__ . '/' .$className.'.php')) {
            $namespase = true;
            $className = str_replace('\\', '/', $className);
        }


            self::$lastLoadedFilename = __DIR__ . "/".$className.".php";

        if (is_readable(self::$lastLoadedFilename))
            require_once(self::$lastLoadedFilename);

    }
}
