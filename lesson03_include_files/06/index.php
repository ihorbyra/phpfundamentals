<?php
// приклад реалізації
// шукаємо файли в поточній директорії
function myAutoload($classname) {
    $filename = $classname .".php";
    include_once($filename);
}

// реєструємо завантажувач
spl_autoload_register('myAutoload');

// створюємо клас
$obj = new myClass();