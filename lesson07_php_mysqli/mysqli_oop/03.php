<?php

// Підготовлені вирази з заданими параметрами та результатами

$conf = require_once 'conf/db.php';
$db = new mysqli($conf['db']['hostname'], $conf['db']['username'], $conf['db']['password'], $conf['db']['database']);

// Створити запит.
$query = "SELECT * FROM goods WHERE id = ?";

// Підготувати запит на сервері MySQL.
$stmt = $db->prepare($query);

$stmt->bind_param('i', $id);
$id = 1;

// Запустити запит
$stmt->execute();

// Оприділити змінні для результату
$stmt->bind_result($id, $name);

// Вибрати і вивести значення
$stmt->fetch();
echo $name.'<br>';

// Завершити запит
$stmt->close();

$db->close();