<?php

// Підготовлені вирази з заданими результатами

$conf = require_once 'conf/db.php';
$db = new mysqli($conf['db']['hostname'], $conf['db']['username'], $conf['db']['password'], $conf['db']['database']);

// Створити запит.
$query = "SELECT * FROM goods";

// Підготувати запит на сервері MySQL.
$stmt = $db->prepare($query);

// Запустити запит
$stmt->execute();

// приділити змінні для результату
$stmt->bind_result($id, $name);

// Вибрати і вивести значення
while ($stmt->fetch()) {
    echo $name.'<br>';
}
// Завершити запит
$stmt->close();

$db->close();
