<?php
$conf = require_once 'conf/db.php';

$db = new mysqli($conf['db']['hostname'], $conf['db']['username'], $conf['db']['password'], $conf['db']['database']);

$query = "SELECT * FROM goods";
$result = $db->query($query);

while ($row = $result->fetch_assoc()) {
    echo $row['name'].'<br>';
}

echo '---------------------------------------------------------------------------------------------------------------<br>';

// Альтернативний варіант

$query = "SELECT * FROM goods";
$result = $db->query($query);
$data = $result->fetch_all(MYSQLI_ASSOC);

foreach ($data as $item => $value) {
    echo $value['name'].'<br>';
}

$db->close();