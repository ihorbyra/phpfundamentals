<?php

// Підготовлені вирази з заданими параметрами та результатами

$conf = require_once 'conf/db.php';
$db = new mysqli($conf['db']['hostname'], $conf['db']['username'], $conf['db']['password'], $conf['db']['database']);

$query = "INSERT INTO goods (id, name) VALUES (?, ?)";

$stmt = $db->prepare($query);

$stmt->bind_param('is', $id, $name);
$id = null;
$name = 'New test product';

$stmt->execute();

echo '<p>ID створеного запису: ' . $db->insert_id  . "</p>";

// Завершити запит
$stmt->close();

$db->close();
