<?php

// Підготовлені вирази з заданими параметрами та результатами

$conf = require_once 'conf/db.php';
$db = new mysqli($conf['db']['hostname'], $conf['db']['username'], $conf['db']['password'], $conf['db']['database']);

$query = "UPDATE goods SET name = ? WHERE id = ?";

$stmt = $db->prepare($query);

$stmt->bind_param('si', $id, $name);
$id = 6;
$name = 'New updated test product';

$stmt->execute();

$stmt->close();

$db->close();

