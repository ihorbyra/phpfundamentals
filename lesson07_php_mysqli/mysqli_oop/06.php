<?php

// Підготовлені вирази з заданими параметрами та результатами

$conf = require_once 'conf/db.php';
$db = new mysqli($conf['db']['hostname'], $conf['db']['username'], $conf['db']['password'], $conf['db']['database']);

$query = "DELETE FROM goods WHERE id = ?";

$stmt = $db->prepare($query);

$stmt->bind_param('i', $id);
$id = 6;

$stmt->execute();

$stmt->close();

$db->close();
