<?php

// Підготовлені вирази з заданими параметрами та результатами

$conf = require_once 'conf/db.php';
$db = mysqli_connect($conf['db']['hostname'], $conf['db']['username'], $conf['db']['password'], $conf['db']['database']);

$query = "INSERT INTO goods (id, name) VALUES (?, ?)";

$stmt = mysqli_prepare($db, $query);

mysqli_stmt_bind_param($stmt, 'is', $id, $name);
$id = null;
$name = 'New test product';

mysqli_stmt_execute($stmt);

echo '<p>ID створеного запису: ' . mysqli_insert_id($db) . "</p>";

mysqli_stmt_close($stmt);

mysqli_close($db);
