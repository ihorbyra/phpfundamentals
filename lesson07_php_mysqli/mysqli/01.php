<?php
$conf = require_once 'conf/db.php';

$db = mysqli_connect($conf['db']['hostname'], $conf['db']['username'], $conf['db']['password'], $conf['db']['database']);

$query = "SELECT * FROM goods";
$result = mysqli_query($db, $query);

while ($row = mysqli_fetch_assoc($result)) {
    echo $row['name'].'<br>';
}

echo '---------------------------------------------------------------------------------------------------------------<br>';

// Альтернативний варіант

$query = "SELECT * FROM goods";
$result = mysqli_query($db, $query);
$data = mysqli_fetch_all($result, MYSQLI_ASSOC);

foreach ($data as $item => $value) {
    echo $value['name'].'<br>';
}

mysqli_close($db);