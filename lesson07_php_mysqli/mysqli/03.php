<?php

// Підготовлені вирази з заданими параметрами та результатами

$conf = require_once 'conf/db.php';
$db = mysqli_connect($conf['db']['hostname'], $conf['db']['username'], $conf['db']['password'], $conf['db']['database']);

// Створити запит.
$query = "SELECT * FROM goods WHERE id = ?";

// Підготувати запит на сервері MySQL.
$stmt = mysqli_prepare($db, $query);

mysqli_stmt_bind_param($stmt, 'i', $id);
$id = 1;

// Запустити запит
mysqli_stmt_execute($stmt);

// Оприділити змінні для результату
mysqli_stmt_bind_result($stmt, $id, $name);

// Вибрати і вивести значення
mysqli_stmt_fetch($stmt);
echo $name.'<br>';

// Завершити запит
mysqli_stmt_close($stmt);

mysqli_close($db);
