<?php

// Підготовлені вирази з заданими результатами

$conf = require_once 'conf/db.php';
$db = mysqli_connect($conf['db']['hostname'], $conf['db']['username'], $conf['db']['password'], $conf['db']['database']);

// Створити запит.
$query = "SELECT * FROM goods";

// Підготувати запит на сервері MySQL.
$stmt = mysqli_prepare($db, $query);

// Запустити запит
mysqli_stmt_execute($stmt);

// приділити змінні для результату
mysqli_stmt_bind_result($stmt, $id, $name);

// Вибрати і вивести значення
while (mysqli_stmt_fetch($stmt)) {
    echo $name.'<br>';
}
// Завершити запит
mysqli_stmt_close($stmt);

mysqli_close($db);
