<?php

// Підготовлені вирази з заданими параметрами та результатами

$conf = require_once 'conf/db.php';
$db = mysqli_connect($conf['db']['hostname'], $conf['db']['username'], $conf['db']['password'], $conf['db']['database']);

$query = "UPDATE goods SET name = ? WHERE id = ?";

$stmt = mysqli_prepare($db, $query);

mysqli_stmt_bind_param($stmt, 'si', $name, $id);
$id = 6;
$name = 'New updated test product';

mysqli_stmt_execute($stmt);

mysqli_stmt_close($stmt);

mysqli_close($db);
