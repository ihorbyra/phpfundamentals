--
-- Структура таблиці `characteristics`
--

CREATE TABLE IF NOT EXISTS `characteristics` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `characteristics`
--

INSERT INTO `characteristics` (`id`, `name`) VALUES
(1, 'Матеріал'),
(2, 'Розміри'),
(3, 'Вага'),
(4, 'Колір'),
(5, 'Гарантія'),
(6, 'Додаткові характеристики');

-- --------------------------------------------------------

--
-- Структура таблиці `characteristic_values`
--

CREATE TABLE IF NOT EXISTS `characteristic_values` (
  `id` int(10) unsigned NOT NULL,
  `characteristicId` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `characteristic_values`
--

INSERT INTO `characteristic_values` (`id`, `characteristicId`, `name`) VALUES
(1, 1, 'АБС-пластик / пластик'),
(2, 1, 'Полиэстер'),
(3, 1, 'Поліпропилен'),
(4, 1, 'Макролон (Полікарбонат)'),
(5, 1, 'Поліестер 600D'),
(6, 2, '47 х 35 х 24 см'),
(7, 2, '71 х 46 х 31 см'),
(8, 2, '66 x 42 x 30 см'),
(10, 3, '3 кг'),
(11, 3, '5 кг'),
(12, 3, '10 кг'),
(13, 3, '7 кг'),
(14, 3, '25 кг'),
(15, 4, 'Фіолетовий'),
(16, 4, 'Чорний'),
(17, 4, 'Червоний'),
(18, 4, 'Зелений'),
(19, 100, '40 л'),
(20, 100, '90 л');

-- --------------------------------------------------------

--
-- Структура таблиці `goods`
--

CREATE TABLE IF NOT EXISTS `goods` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `goods`
--

INSERT INTO `goods` (`id`, `name`) VALUES
(1, 'Валіза Bernard Comfort Alum 20'),
(2, 'Валіза Puccini Camerino'),
(3, 'Чемодан Roncato Flexi'),
(4, 'Валіза Heys xcase 2G M'),
(5, 'Валіза Members Hi-Lite');

-- --------------------------------------------------------

--
-- Структура таблиці `goods_characteristics`
--

CREATE TABLE IF NOT EXISTS `goods_characteristics` (
  `id` int(10) unsigned NOT NULL,
  `goodsId` int(10) unsigned NOT NULL,
  `characteristicId` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `goods_characteristics`
--

INSERT INTO `goods_characteristics` (`id`, `goodsId`, `characteristicId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 1),
(7, 2, 2),
(8, 2, 3),
(9, 3, 3),
(10, 4, 4);

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `characteristics`
--
ALTER TABLE `characteristics`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `characteristic_values`
--
ALTER TABLE `characteristic_values`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `goods`
--
ALTER TABLE `goods`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `goods_characteristics`
--
ALTER TABLE `goods_characteristics`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `characteristics`
--
ALTER TABLE `characteristics`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблиці `characteristic_values`
--
ALTER TABLE `characteristic_values`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблиці `goods`
--
ALTER TABLE `goods`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблиці `goods_characteristics`
--
ALTER TABLE `goods_characteristics`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;