<?php
class Mysql extends mysqli{

	public function __construct($conf) {

	    parent::__construct($conf['db']['hostname'], $conf['db']['username'], $conf['db']['password'], $conf['db']['database']);

        if (mysqli_connect_error()) {
            die('Помилка підключення (' . mysqli_connect_errno() . ') '
                    . mysqli_connect_error());
        }
        parent::set_charset("utf8");
        $this->query("SET NAMES utf8");
    }
    
	public function execute($query, $resultmode = MYSQLI_STORE_RESULT){
		$query = parent::query($query, $resultmode);
		if(!$query){
			throw new Exception("Database Error [errno: {$this->errno}, connect_errno {$this->connect_errno}]");
		}
		return $query;
	}
}