-- inner join:
SELECT c.name, v.name
FROM characteristics c
INNER JOIN characteristic_values v
ON c.id = v.characteristicId

-- left join:
SELECT c.name, v.name
FROM characteristics c
LEFT JOIN characteristic_values v
ON c.id = v.characteristicId

-- right join:
SELECT c.name, v.name
FROM characteristics c
RIGHT JOIN characteristic_values v
ON c.id = v.characteristicId

-- outer join:
SELECT c.name, v.name
FROM characteristics c
LEFT JOIN characteristic_values v
ON c.id = v.characteristicId
UNION
SELECT c.name, v.name
FROM characteristics c
RIGHT JOIN characteristic_values v
ON c.id = v.characteristicId

-- LEFT EXCLUDING JOIN:
SELECT c.name, v.name
FROM characteristics c
LEFT JOIN characteristic_values v
ON c.id = v.characteristicId
WHERE v.characteristicId IS NULL
RIGHT EXCLUDING JOIN:
SELECT c.name, v.name
FROM characteristics c
RIGHT JOIN characteristic_values v
ON c.id = v.characteristicId
WHERE c.id IS NULL

-- OUTER EXCLUDING JOIN:
-- https://www.xaprb.com/blog/2006/05/26/how-to-write-full-outer-join-in-mysql/
SELECT c.name, v.name
FROM characteristics c
LEFT JOIN characteristic_values v
ON c.id = v.characteristicId
WHERE v.characteristicId IS NULL
UNION
SELECT c.name, v.name
FROM characteristics c
RIGHT JOIN characteristic_values v
ON c.id = v.characteristicId
WHERE c.id IS NULL

-- CROSS JOIN:
SELECT characteristics.name, characteristic_values.name
FROM characteristics, characteristic_values
WHERE characteristics.id=characteristic_values.characteristicId
