<?php

class Employee
{
    private $lastName;
    private $firstName;

    public function __construct($firstName, $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function rename($firstName, $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName . '<br>';
    }
}

$employee =  new Employee('Rasmus', 'Lerdorf');

$employee->status = 2;
echo 'Status: ' . $employee->status . '<br>';

echo $employee->getFullName() . '<br>';