<?php

class Employee
{
    private $lastName;
    private $firstName;

    //public $status = 'my new status';

    public function __construct($firstName, $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function __get($name)
    {
        echo 'Get: ' . $name . '<br>';
    }

    public function rename($firstName, $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName . '<br>';
    }
}

$employee =  new Employee('Rasmus', 'Lerdorf');

echo $employee->status . '<br>';

echo $employee->getFullName() . '<br>';