<?php

class Employee
{
    private $lastName;
    private $firstName;

    private $attributes = [];

    public function __construct($firstName, $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function __get($name)
    {
        return isset($this->attributes[$name]) ? $this->attributes[$name] : null;
    }

    public function __set($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    /*public function __isset($name)
    {
        return isset($this->attributes[$name]);
    }

    public function __unset($name)
    {
        unset($this->attributes[$name]);
    }*/

    public function rename($firstName, $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName . '<br>';
    }
}

$employee =  new Employee('Rasmus', 'Lerdorf');

// показати приклади з __unset, __isset  і без
$employee->status = 2;
echo $employee->status . '<br>';

echo 'Is set? : ' . ( isset($employee->status) ?: 0 ) . '<br>';

unset($employee->status);
echo $employee->status . '<br>';

echo $employee->getFullName() . '<br>';