<?php
$conf = require_once 'conf/db.php';

try {
    # MS SQL Server і Sybase через PDO_DBLIB
    $dbh = new PDO("mysql:host={$conf['db']['hostname']};dbname={$conf['db']['database']}", $conf['db']['username'], $conf['db']['password']);
}
catch(PDOException $e) {
    echo $e->getMessage();
}

###################################################################################

// FETCH_ASSOC

# оскільки це звичайний запит без placeholder’ів,
# можна зразу використовувати метод query()
$sth = $dbh->query('SELECT * from goods');

# встановлюємо режим вибірки
$sth->setFetchMode(PDO::FETCH_ASSOC);

# виводимо дані
while($row = $sth->fetch()) {
    echo $row['name'] . "<br>";
}

###################################################################################
echo "------------------------------------------------------------------------<br>";

// FETCH_OBJ

# створюємо запит
$sth = $dbh->query('SELECT * from goods');

# встановлюємо режим вибірки
$sth->setFetchMode(PDO::FETCH_OBJ);

# виводимо дані
while($row = $sth->fetch()) {
    echo $row->name . "<br>";
}

###################################################################################
echo "------------------------------------------------------------------------<br>";

class Secret
{
    public $name;

    public function __construct($other = '')
    {
        $this->name = preg_replace('/[a-z]/', 'x', $this->name);
    }
}

$sth = $dbh->query('SELECT name from goods');
$sth->setFetchMode(PDO::FETCH_CLASS, 'Secret');

while($obj = $sth->fetch()) {
    echo $obj->name . "<br>";
}