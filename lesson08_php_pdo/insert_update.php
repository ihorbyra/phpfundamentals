<?php

$conf = require_once 'conf/db.php';

try {
    # MS SQL Server і Sybase через PDO_DBLIB
    $dbh = new PDO("mysql:host={$conf['db']['hostname']};dbname={$conf['db']['database']}", $conf['db']['username'], $conf['db']['password']);
}
catch(PDOException $e) {
    echo $e->getMessage();
}

// Без placeholders - двері SQL-ін'єкцій відкриті!
$withOutPlaceholders = $dbh->prepare("INSERT INTO goods (name) values ($name)");

// Безіменні placeholders
$noNamePlaceholders = $dbh->prepare("INSERT INTO goods (id, name) values (?, ?)");

// Іменні placeholders
$placeholders = $dbh->prepare("INSERT INTO goods (id, name) values (:id, :name)");

###################################################################################

// Безіменні placeholders

# назначаємо змінну кожному placeholder"у, з індексати 1 і 2
$noNamePlaceholders->bindParam(1, $id);
$noNamePlaceholders->bindParam(2, $name);

# Створюємо перший запис
$id = null;
$name = 'New PDO product with ? placeholders';
//$noNamePlaceholders->execute();

# Створюємо другий запис і т.д.
$id = null;
$name = 'New PDO product with ? placeholders 2';
//$noNamePlaceholders->execute();

###################################################################################

// Безіменні placeholders - передача даних з масиву

# набір даних, які ми будемо вставляти
$data = [null, 'New PDO product with ? placeholders 3'];
//$noNamePlaceholders->execute($data);

###################################################################################
####################################################################################
####################################################################################

// Іменні placeholders

# Першим аргументом є ім'я placeholder'а
# Його прийнято починати з двокрапки
# Хоча працює і без них
$placeholders->bindParam(':name', $name);

# набір даних, які ми будемо вставляти
$data = ['id' => null, 'name' => 'New PDO product with NAMED placeholders'];
$placeholders = $dbh->prepare("INSERT INTO goods (id, name) values (:id, :name)");
$placeholders->execute($data);