<?php

$conf = require_once 'conf/db.php';

try {
    # MS SQL Server і Sybase через PDO_DBLIB
    $dbh = new PDO("mysql:host={$conf['db']['hostname']};dbname={$conf['db']['database']}", $conf['db']['username'], $conf['db']['password']);
}
catch(PDOException $e) {
    echo $e->getMessage();
}


$dbh->lastInsertId();

$dbh->exec('DELETE FROM goods');
$dbh->exec("SET time_zone = '-8:00'");

$safe = $dbh->quote($unsafe);

