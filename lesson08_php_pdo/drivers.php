<?php

// список доступних драйверів
print_r(PDO::getAvailableDrivers());

// підключення
try {
    # MS SQL Server і Sybase через PDO_DBLIB
    $dbh = new PDO("mssql:host=$host;dbname=$dbname", $user, $pass);
    $dbh = new PDO("sybase:host=$host;dbname=$dbname", $user, $pass);

    # MySQL через PDO_MYSQL
    $dbh = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);

    # SQLite
//    $dbh = new PDO("sqlite:path_to_database/database.db");
}
catch(PDOException $e) {
    echo $e->getMessage();
}

// закрити підключання
$dbh = null;

// Режими помилок
$dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT );
$dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
$dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );